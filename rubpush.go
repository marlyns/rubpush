package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"runtime"
	"sync"
	"time"

	"github.com/sideshow/apns2"
	"github.com/sideshow/apns2/certificate"
	"github.com/sideshow/apns2/payload"

	"bitbucket.org/Valrox/rubpush/qparse"
	"bytes"
)

var micex bool
var thisparse bool
var mobile bool
var yahoo bool
var on bool
var off bool

type ApnsPayload struct {
	Aps struct {
		    Alert string `json:"alert"`
		    Badge int    `json:"badge"`
		    Sound string `json:"sound"`
	    } `json:"aps"`
}

func requestHandler(w http.ResponseWriter, r *http.Request) {
	m := r.FormValue("m")
	if m != "" {
		if m == "1" {
			micex = true
			fmt.Fprintf(w, "MICEX ON")

		} else {
			micex = false
			fmt.Fprintf(w, "MICEX OFF")
		}
	}

	t := r.FormValue("t")
	if t != "" {
		if t == "1" {
			thisparse = true
			fmt.Fprintf(w, "thisparse ON")

		} else {
			thisparse = false
			fmt.Fprintf(w, "thisparse OFF")
		}
	}

	mob := r.FormValue("mob")
	if mob != "" {
		if mob == "1" {
			mobile = true
			fmt.Fprintf(w, "mobile inv ON")

		} else {
			mobile = false
			fmt.Fprintf(w, "mobile inv OFF")
		}
	}
	onrequare := r.FormValue("on")
	if onrequare != "" {
		if onrequare == "1" {
			on = true
			fmt.Fprintf(w, "on qoute parse TRUE")

		} else {
			on = false
			fmt.Fprintf(w, "on qoute parse FALSE")
		}
	}
	offrequare := r.FormValue("off")
	if offrequare != "" {
		if offrequare == "1" {
			off = true
			fmt.Fprintf(w, "off qoute parse ON")

		} else {
			off = false
			fmt.Fprintf(w, "off qoute parse OFF")
		}
	}
	yahooreq := r.FormValue("y")
	if yahooreq != "" {
		if yahooreq == "1" {
			yahoo = true
			fmt.Fprintf(w, "yahoo qoute parse ON")

		} else {
			yahoo = false
			fmt.Fprintf(w, "yahoo qoute parse OFF")
		}
	}
}

func addStringToLog(format string, a ...interface{}) {
	//mux.Lock()
	//
	//f, err := os.OpenFile("./pushlog.dat", os.O_APPEND | os.O_WRONLY | os.O_CREATE, 0666)
	//if err != nil {
	//	fmt.Printf("%s", err)
	//}
	//
	//defer f.Close()
	//
	//loc, err := time.LoadLocation("Europe/Moscow")
	//if err != nil {
	//	fmt.Println("err: ", err.Error())
	//}
	//t := time.Now().In(loc)
	//
	//var timestr string = t.Format(time.RFC3339)
	//
	//if _, err = f.WriteString(fmt.Sprintf("%s: %s\n", timestr, text)); err != nil {
	//	fmt.Printf("%s", err)
	//}
	//
	//mux.Unlock()

	if a != nil {
		fmt.Println(fmt.Sprintf(format, a...))
		//fmt.Printf("\n")
		log.Println(fmt.Sprintf(format, a...))
		//log.Printf("\n")
	} else
	{
		fmt.Print(fmt.Sprint(format))
		fmt.Printf("\n")
		log.Print(fmt.Sprint(format))
		log.Printf("\n")
	}
}

func sendPush(client *apns2.Client, token string, body string, badge int, identifier int) {

	//defer func() { <-sem }()
	//fmt.Println(token)
	notifPayload := payload.NewPayload().Alert(body).Badge(badge).Sound("default")
	//notifPayload := payload.NewPayload().ContentAvailable()
	notification := &apns2.Notification{}
	notification.DeviceToken = token
	notification.Topic = "com.dialect.flappyruble"
	notification.Payload = notifPayload

	res, err := client.Push(notification)

	if err != nil {
		log.Fatal("Error:", err)
	}

	if res.Sent() {
		//log.Println("Sent:", res.ApnsID)
	} else {
		addStringToLog("Not Sent: %v %v %v\n", res.StatusCode, res.ApnsID, res.Reason)
		if res.StatusCode == 410 ||
			res.Reason == apns2.ReasonBadDeviceToken ||
			res.Reason == apns2.ReasonUnregistered ||
			res.Reason == apns2.ReasonDeviceTokenNotForTopic {
			badTokens <- token
		}
	}
}

func prepareSendingPush() {
	addStringToLog("Start get tokens %d", runtime.NumGoroutine())
	//if runtime.NumGoroutine() > 200 {
	//panic(fmt.Sprintf("%v", runtime.NumGoroutine()))
	//}
	response, err := http.Get("https://i-dialect.com/rubl/gettokens.php")
	if err != nil {
		addStringToLog("%v", err)
	} else {
		defer response.Body.Close()
		contents, err := ioutil.ReadAll(response.Body)
		if err != nil {
			addStringToLog("%v", err)
		}
		type JsonObj struct {
			Status string   `json:"status"`
			Text   string   `json:"text"`
			Tokens []string `json:"tokens"`
		}
		js := new(JsonObj)
		err = json.Unmarshal(contents, &js)
		if err != nil {
			addStringToLog("error: %v", err)
		}
		if len(js.Tokens) > 0 {
			cert, err := certificate.FromPemFile("./resources/pushcert.pem", "lumma") // /root/go/src/bitbucket.org/Valrox/rubpush/resources/pushcert.pem
			if err != nil {
				addStringToLog("Cert Error:", err)
				log.Fatal("Cert Error:", err)
			}
			client := apns2.NewClient(cert).Production()
			if err != nil {
				log.Fatal("Error:", err)
			}
			addStringToLog("Start sending")
			for i := 0; i < len(js.Tokens); i++ {
				tok := js.Tokens[i]
				//if tok != "" && tok == "fdcfc568736addb62755c0ff2b71c43aeaf4f90319ed24458aae81e68748ed39" {
				if tok != "" {
					//fmt.Println(tok)
					//sem <- true
					go sendPush(client, tok, js.Text, 1, i)
				}
			}
			//for i := 0; i < cap(sem); i++ {
			//	sem <- true
			//}
			addStringToLog("End sending")
		}
		addStringToLog("End get tokens")
	}
}

func canStartRequst() bool  {
	if on {
		return true
	}
	if off {
		return false
	}
	loc, err := time.LoadLocation("Europe/Moscow")
	if err != nil {
		fmt.Println("err: ", err.Error())
		return true
	}
	t := time.Now().In(loc)
	if t.Weekday() == time.Sunday {
		return false
	} else if  t.Weekday() == time.Saturday {
		if t.Hour() >= 3 {
			return false
		} else {
			return true
		}

	} else if t.Weekday() == time.Monday {
		if t.Hour() >= 6 {
			return true
		} else {
			return false
		}
	} else if t.Hour() >= 3 && t.Hour() <= 6 {
		return false
	}
	return true
}

func startRequestQuotes() {
	addStringToLog("Start get quotes")
	response, err := http.Get("https://i-dialect.com/rubl/quotes/qrec7.php")
	if err != nil {
		addStringToLog("%s", err)
	} else {
		defer response.Body.Close()
		addStringToLog("End get quotes")
	}
}

func sendBadTokens(tokens []string)  {
	jsonData, _ := json.Marshal(tokens)
	if jsonData != nil {
		req, err := http.NewRequest("POST", "https://i-dialect.com/rubl/feedback-new.php", bytes.NewBuffer(jsonData))
		req.Header.Set("Content-Type", "application/json")
		req.Header.Set("User-Agent", "Mozilla/5.0 (iPad; U; CPU OS 9_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B")

		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			fmt.Printf("%s", err)
		} else {
			defer resp.Body.Close()

			addStringToLog("Response bad tokens status: %s", resp.Status)
			//fmt.Println("response Headers:", resp.Header)
			body, _ := ioutil.ReadAll(resp.Body)
			fmt.Println("\nResponse Body:", string(body))
		}
	}
}

var mux sync.Mutex
var badTokens = make(chan string)
//var concurrency = 5
//var sem = make(chan bool, concurrency)

func main() {
	thisparse = true
	micex = true
	mobile = false
	on = false
	off = false
	yahoo = false

	counterChan := 0

	f, err := os.OpenFile("./rubpush.log", os.O_APPEND | os.O_CREATE | os.O_RDWR, 0666)
	if err != nil {
		fmt.Printf("error opening file: %v", err)
	}

	// don't forget to close it
	defer f.Close()

	// assign it to the standard logger
	log.SetOutput(f)

	go func() {
		fmt.Println("Listen port 8080")
		http.HandleFunc("/", requestHandler)
		err := http.ListenAndServe(":8080", nil)
		if err != nil {
			fmt.Println("ListenAndServe: ", err)
		}
	} ()
	//cert, err := certificate.FromPemFile("./resources/pushcert.pem", "lumma") // /root/go/src/bitbucket.org/Valrox/rubpush/resources/pushcert.pem
	//if err != nil {
	//	addStringToLog("Cert Error:", err)
	//	log.Fatal("Cert Error:", err)
	//}

	apns2.HTTPClientTimeout = 0
	//client := apns2.NewClient(cert).Production()
	//if err != nil {
	//	log.Fatal("Error:", err)
	//}
	prepareSendingPush()
	tickChan := time.NewTicker(time.Second * 13).C
	tickRec := time.NewTicker(time.Second * 20).C
	tickBadTokens := time.NewTicker(time.Second * 25).C

	badTokensList := []string{}

	for {
		select {
		case <-tickChan:
			{
				if canStartRequst() {
					prepareSendingPush()
				} else {
					//addStringToLog("not can start request: sending push")
				}
			}
		case <-tickRec:
			{
				if canStartRequst() {
					if thisparse {
						suc, m := qparse.StartParseQuotes(!micex, mobile, yahoo)
						addStringToLog("Parse quotes suc: %t, micex %t, mobile %t", suc, m, mobile)
					} else {
						go startRequestQuotes()
					}
				} else {
					//addStringToLog("not can start request: quotes")
				}
			}
		case token := <-badTokens:
			{
				addStringToLog(token)
				badTokensList = append(badTokensList, token)
			}
		case <-tickBadTokens:
			{
				counterChan++
				if len(badTokensList) >= 100 {
					go func(list []string) {
						//request
						addStringToLog("Bad tokens for sending: counter %d, len list %d", counterChan, len(list))
						sendBadTokens(list)
					}(badTokensList[:100])
					badTokensList = badTokensList[100:]
				} else
				{
					if len(badTokensList) != 0 {
						countList := len(badTokensList)
						go func(list []string) {
							//request
							addStringToLog("Bad tokens for sending: counter %d, len list %d", counterChan, len(list))
							sendBadTokens(list)
						}(badTokensList[:countList])
						badTokensList = badTokensList[countList:]
					}
				}
			}
		}
	}
}
