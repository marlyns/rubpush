package qparse
import (
	"strings"
	"fmt"
	"regexp"
	"net/http"
	"io/ioutil"
	"encoding/json"
	"strconv"
	"bytes"
	"time"
	"github.com/opesun/goquery"
)

var quoteNames = map[string]string{
	"USD/EUR":              "USDEUR",
	"USD/RUB":           "USDRUB",
	"USD/UAH":                  "USDUAH",
	"USD/GBP":           "USDGBP",
	"USD/BYR":              "USDBYR",
	"USD/TRY":         "USDTRY",
	"USD/CNY":               "USDCNY",
	"USD/JPY":        "USDJPY",
	"USD/THB":            "USDTHB",
	"USD/INR":    "USDINR",
	"USD/CHF":              "USDCHF",
	"USD/CZK":          "USDCZK",
	"USD/KZT": 			"USDKZT",
	"USD/AED":            "USDAED",
	"USD/CAD":            "USDCAD",
	"USD/BTC":            "USDBTC",
}

type QuoteInv struct  {
	Last string
	High string
	Low string
	Change string
	Name string
}

type JsonYQL struct {
	Query struct {
			  Results struct {
						  Quote []struct {
							  AverageDailyVolume   interface{} `json:"AverageDailyVolume"`
							  Change               string      `json:"Change"`
							  DaysHigh             string      `json:"DaysHigh"`
							  DaysLow              string      `json:"DaysLow"`
							  DaysRange            string      `json:"DaysRange"`
							  LastTradePriceOnly   string      `json:"LastTradePriceOnly"`
							  MarketCapitalization interface{} `json:"MarketCapitalization"`
							  Name                 string      `json:"Name"`
							  StockExchange        string      `json:"StockExchange"`
							  Symbol               string      `json:"Symbol"`
							  Volume               string      `json:"Volume"`
							  YearHigh             string      `json:"YearHigh"`
							  YearLow              string      `json:"YearLow"`
						  } `json:"quote"`
					  } `json:"results"`
		  } `json:"query"`
}

type JsonMicex struct {
	Header struct {
			   High                 float64     `json:"high"`
			   Last                 float64     `json:"last"`
			   LastChangePercent    float64     `json:"last_change_percent"`
			   Low                  float64     `json:"low"`
		   } `json:"header"`
}

type JsonRes struct {
	QuoteRes []struct {
		Change               string
		High             	 string
		Low              	 string
		LastTradePriceOnly   string
		Name                 string
	}
}

func parse(start string, finish string, content string) string  {
	position := strings.Index(content, start)
	if position == -1 {
		return ""
	}
	content = content[(position + len(start)):]
	position = strings.Index(content, finish)
	if position == -1 {
		return ""
	}
	content = strings.Replace(content, ",", "", -1)
	content = content[:position]
	re := regexp.MustCompile(`[-+]?(\d*[.])?\d+`)
	result := re.FindString(content)
	return result
}

func parseTwo(start string, finish string, content string) (string, string)  {
	position := strings.Index(content, start)
	if position == -1 {
		return "", ""
	}
	content = content[(position + len(start)):]
	position = strings.Index(content, finish)
	if position == -1 {
		return "", ""
	}
	content = strings.Replace(content, ",", "", -1)
	content = content[:position]
	re := regexp.MustCompile(`[-+]?(\d*[.])?\d+`)
	twoquote := re.FindAllString(content, 2)
	if len(twoquote) == 2 {
		return twoquote[0], twoquote[1]
	}
	return "", ""
}

type Quote struct  {
	Last string
	High string
	Low string
	Change string
}

func getProductQuotesWithKey(quotetype string, name string, key string) *Quote {
	req, err := http.NewRequest("GET", fmt.Sprintf("http://m.investing.com/%s/%s", quotetype, name), nil)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("User-Agent", "Mozilla/5.0 (iPad; U; CPU OS 9_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B")

	timeout := time.Duration(5 * time.Second)
	client := &http.Client{
		Timeout: timeout,
	}
	response, err := client.Do(req)
	if err == nil {
		defer response.Body.Close()
		contents, err := ioutil.ReadAll(response.Body)
		if err == nil {
			//n := bytes.IndexByte(contents, 0)
			respStr := string(contents[:])
			result := new(Quote)
			start := fmt.Sprintf("pid-%s-last\">", key)
			finish := "</span>"
			result.Last = parse(start, finish, respStr)

			start = fmt.Sprintf("pid-%s-pcp\">", key)
			finish = "</i>"
			result.Change = parse(start, finish, respStr)

			start = "Days High</td>"
			finish = "</td>"
			result.High = parse(start, finish, respStr)

			start = "Days Low</td>"
			finish = "</td>"
			result.Low = parse(start, finish, respStr)

			if result.Last != "" && result.Change != "" && result.High != "" && result.Low != "" {
				return result
			}
			return nil
		} else {
			fmt.Println(err)
		}

	}
	return nil
}

func getProductQuotesWithKeyNew(quotetype string, name string, key string) *Quote {
	req, err := http.NewRequest("GET", fmt.Sprintf("http://investing.com/%s/%s", quotetype, name), nil)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2")

	timeout := time.Duration(5 * time.Second)
	client := &http.Client{
		Timeout: timeout,
	}
	response, err := client.Do(req)
	if err == nil {
		defer response.Body.Close()
		contents, err := ioutil.ReadAll(response.Body)
		if err == nil {
			//n := bytes.IndexByte(contents, 0)
			respStr := string(contents[:])
			result := new(Quote)
			start := fmt.Sprintf("inlineblock pid-%s-last", key)
			finish := "</span>"
			result.Last = parse(start, finish, respStr)
			start = fmt.Sprintf("pid-%s-pcp parentheses", key)
			finish = "</span>"
			result.Change = parse(start, finish, respStr)

			//			start = "Day's Range:</span>"
			//			finish = "</span>"
			//			result.Low, result.High = parseTwo(start, finish, respStr)

			start = "pid-" + key + "-high"
			finish = "</span>"
			result.High = parse(start, finish, respStr)

			start = "pid-" + key + "-low"
			finish = "</span>"
			result.Low = parse(start, finish, respStr)


			if result.Last != "" && result.Change != "" && result.High != "" && result.Low != "" {
				return result
			}
			return nil
		} else {
			fmt.Println(err)
		}

	}
	return nil
}

func getMicexQuoteWithKey (name string) *Quote {
	req, err := http.NewRequest("GET", fmt.Sprintf("http://stock.quote.rbc.ru/demo/selt.0/intraday/%s.rus.js?format=json", name), nil)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2")

	client := &http.Client{}
	response, err := client.Do(req)
	if err == nil {
		defer response.Body.Close()
		contents, err := ioutil.ReadAll(response.Body)
		if err == nil {
			respStr := string(contents[:])
			respStr = strings.Replace(respStr, "try{ var xdata = [", "", -1)
			respStr = strings.Replace(respStr, "]; jsonp(xdata); } catch(e){}", "", -1)
			result := new(Quote)
			jsMicex := new(JsonMicex)
			err = json.Unmarshal([]byte(respStr), &jsMicex)
			if err != nil {
				fmt.Println(err)
			} else {
				if jsMicex.Header.Last > 0 {
					result.Last = strconv.FormatFloat(jsMicex.Header.Last, 'f', -1, 64)
					result.Change = strconv.FormatFloat(jsMicex.Header.LastChangePercent, 'f', -1, 64)
					result.High = strconv.FormatFloat(jsMicex.Header.High, 'f', -1, 64)
					result.Low = strconv.FormatFloat(jsMicex.Header.Low, 'f', -1, 64)

					if result.Last != "" && result.Change != "" && result.High != "" && result.Low != "" {
						return result
					}
				}
				return nil
			}
		}

	}
	return nil
}

func getResultFromYQL () *JsonYQL {
	response, err := http.Get("https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.quote%20where%20symbol%20in%20(\"EURRUB=X\",\"USDGBP=X\",\"USDRUB=X\",\"USDEUR=X\",\"USDUAH=X\",\"USDTRY=X\",\"USDCNY=X\",\"USDJPY=X\",\"USDCAD=X\",\"USDTHB=X\",\"USDINR=X\",\"USDCHF=X\",\"USDCZK=X\",\"USDKZT=X\",\"USDAED=X\",\"USDBYR=X\")&format=json&env=store://datatables.org/alltableswithkeys&callback=")
	if err == nil {
		js := new(JsonYQL)
		defer response.Body.Close()
		contents, err := ioutil.ReadAll(response.Body)
		if err == nil {
			err = json.Unmarshal(contents, &js)
			fmt.Println(js)
			return js
		}
	}
	return nil
}

func invParse() []*QuoteInv {

	req, err := http.NewRequest("GET", "http://www.investing.com/currencies/single-currency-crosses", nil)
	req.Header.Set("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8")
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2")
	req.Header.Set("Cookie", "__gads=ID=fdb5b2c742285d8a:T=1448661189:S=ALNI_MavSx0nZYjYw2pk97CW6qA8HnEFCQ; _jsuid=962612872; __qca=P0-2101140415-1454401404108; optimizelyEndUserId=oeu1458157464014r0.4777777276467532; userAlertPopupNotificationUntil=2016-04-19T23:25:41.375Z; isUserNoticedNewAlertPopup=1; __unam=ce5f7e1-15470f2f365-740bc12-1; PHPSESSID=ffobnetitlt0534qs9gah6ajr0; geoC=RU; fpros_popup=up; adBlockerNewUserDomains=1469084802; gtmFired=OK; show_big_billboard1=true; editionPostpone=1469087046406; optimizelySegments=%7B%224225444387%22%3A%22gc%22%2C%224226973206%22%3A%22direct%22%2C%224232593061%22%3A%22false%22%2C%225010352657%22%3A%22none%22%7D; optimizelyBuckets=%7B%7D; nyxDorf=NzExZWMrM21kMjoyZis4PGIyNHE3MmFq; _gat=1; _gat_allSitesTracker=1; _ga=GA1.2.659458809.1448661190")

	timeout := time.Duration(10 * time.Second)
	client := &http.Client{
		Timeout: timeout,
	}
	response, err := client.Do(req)
	quotes := []*QuoteInv{}
	if err == nil {
		defer response.Body.Close()
		contents, err := ioutil.ReadAll(response.Body)
		respStr := string(contents[:])

		x, err := goquery.ParseString(respStr)
//		fmt.Println(x)
		if err != nil {
			panic(err)
		}
		n := x.Find("tbody")
		tbody := n.Find("tr")
		counter := 0
		for _, tr := range tbody {
			//		fmt.Println(td.Node.Data)
			if len(tr.Node.Attr) > 0 {
				at := tr.Node.Attr[0]
				re := regexp.MustCompile(`(\d)?\d+`)
				pid := re.FindString(at.Val)
				//			fmt.Println(result)
				if pid != "" {
					bidkey := "pid-" + pid + "-bid"
					askkey := "pid-" + pid + "-ask"
					highkey := "pid-" + pid + "-high"
					lowkey := "pid-" + pid + "-low"
					percentkey := "pid-" + pid + "-pcp"
					var fname string
					var fbid string
					var fask string
					var fhigh string
					var flow string
					var fchange string
					for _, td := range tr.Child {
						for _, tdc := range td.Child {
							for _, tdcc := range tdc.Child {
								if len(tdcc.Data) >= 3 {
									fname = tdcc.Data
								}
							}
						}
						for _, tdattr := range td.Attr {

							if tdattr.Val == bidkey {
								for _, val := range td.Child {
									fbid = val.Data
									//								fmt.Println(fbid)
									break
								}
							} else if (tdattr.Val == askkey) {
								for _, val := range td.Child {
									fask = val.Data
									break
								}
							} else if (tdattr.Val == highkey) {
								for _, val := range td.Child {
									fhigh = val.Data
									break
								}
							} else if (tdattr.Val == lowkey) {
								for _, val := range td.Child {
									flow = val.Data
									break
								}
							} else {
								rstr := fmt.Sprintf(`%s`, percentkey)
								re := regexp.MustCompile(rstr)
								result := re.FindStringIndex(tdattr.Val)
								//							fmt.Println(result)
								if (result != nil) {
									for _, val := range td.Child {
										fchange = val.Data
										break
									}
								}
							}
						}
					}
					//				fmt.Println(fchange)
					if fbid != "" && fask != "" && fhigh != "" && flow != "" && fchange != "" && fname != "" {
						fchange = strings.Replace(fchange, "%", "", -1)
						fbid = strings.Replace(fbid, ",", "", -1)
						fask = strings.Replace(fask, ",", "", -1)
						fhigh = strings.Replace(fhigh, ",", "", -1)
						flow = strings.Replace(flow, ",", "", -1)
						//					fmt.Println(fbid, fask, fhigh, flow, fchange, fname)
						//					fmt.Println(fname)
						bidFloat, _ := strconv.ParseFloat(fbid, 64)
						askFloat, _ := strconv.ParseFloat(fask, 64)
						lastFloat := (bidFloat + askFloat)/2

						highFloat, _ := strconv.ParseFloat(fhigh, 64)
						lowFloat, _ := strconv.ParseFloat(flow, 64)

						changeFloat, _ := strconv.ParseFloat(fchange, 64)
						//					fmt.Println(lastFloat, highFloat, lowFloat, changeFloat)
						s := strings.Split(fname, "/")
						if len(s) == 2 {
							if s[1] == "USD" {

								openFloat := lastFloat/(1 + (changeFloat/100))

								openFloat = 1/openFloat

								lastFloat = 1/lastFloat


								h := 1/highFloat
								l := 1/lowFloat
								highFloat = l
								lowFloat = h
								//							changeFloat = -1*lastFloat*changeFloat
								changeFloat = ((lastFloat/openFloat) - 1)*100
								fname = fmt.Sprintf("%s/%s", s[1], s[0])
							}
						}
						newname, ok := quoteNames[fname]
						if ok {
							q := new(QuoteInv)
							q.Last = strconv.FormatFloat(lastFloat, 'f', 15, 64)
							q.High = strconv.FormatFloat(highFloat, 'f', 15, 64)
							q.Low = strconv.FormatFloat(lowFloat, 'f', 15, 64)
							q.Change = strconv.FormatFloat(changeFloat, 'f', 15, 64)
							q.Name = newname
							exist := false
							for _, current := range quotes {
								//							fmt.Printf("\n%s %s", current.Name, q.Name)
								if current.Name == q.Name {

									//								fmt.Printf("\nEXIST")
									exist = true
									break
								}
							}
							//						fmt.Printf("\n%b", exist)
							if q.Name == "USDRUB" {
								if counter <= 2 {
									exist = true
								}
								counter++
							}
							if !exist {
								//							fmt.Printf("\nappend")
								//							fmt.Printf("\n%s", q.Name)
								quotes = append(quotes, q)
								//							fmt.Println(len(quotes))
							}
						}
					}
				}
			}
		}
	}


	//	fmt.Println(len(quotes))
	return quotes
}


func StartParseQuotes(withoutmicex bool, mobile bool, withyql bool) (succeeded bool, micex bool)  {

	var brent *Quote
	var gold *Quote
	var wti *Quote
	if mobile {
		brent = getProductQuotesWithKey("commodities", "brent-oil", "8833")
		time.Sleep(1 * time.Second)
		gold = getProductQuotesWithKey("commodities", "gold", "8830")
		time.Sleep(1 * time.Second)
		wti = getProductQuotesWithKey("commodities", "crude-oil", "8849")
	} else {
		brent = getProductQuotesWithKeyNew("commodities", "brent-oil", "8833")
		time.Sleep(1 * time.Second)
		gold = getProductQuotesWithKeyNew("commodities", "gold", "8830")
		time.Sleep(1 * time.Second)
		wti = getProductQuotesWithKeyNew("commodities", "crude-oil", "8849")
	}

	time.Sleep(3 * time.Second)

	var jsYQL *JsonYQL
	var invQuote []*QuoteInv
	var countYQL int
	if withyql {
		jsYQL = getResultFromYQL()
		countYQL = len(jsYQL.Query.Results.Quote)
	} else {
		invQuote = invParse()
		countYQL = len(invQuote)
	}

	jsRes := JsonRes{
		QuoteRes:make([]struct {
			Change               string
			High             	 string
			Low              	 string
			LastTradePriceOnly   string
			Name                 string
		},(4 + countYQL)),
	}

	num := 0
	if brent != nil {
		fmt.Printf("\nbrent %s", brent.High)
		jsRes.QuoteRes[num].LastTradePriceOnly = brent.Last
		jsRes.QuoteRes[num].Change = brent.Change
		jsRes.QuoteRes[num].High = brent.High
		jsRes.QuoteRes[num].Low = brent.Low
		jsRes.QuoteRes[num].Name = "USDBBB"
		num++
	}

	if gold != nil {
		fmt.Printf("\ngold %s", gold.Low)
		jsRes.QuoteRes[num].LastTradePriceOnly = gold.Last
		jsRes.QuoteRes[num].Change = gold.Change
		jsRes.QuoteRes[num].High = gold.High
		jsRes.QuoteRes[num].Low = gold.Low
		jsRes.QuoteRes[num].Name = "USDGGG"
		num++
	}

	if wti != nil{
		fmt.Printf("\nwti %s", wti.Low)
		jsRes.QuoteRes[num].LastTradePriceOnly = wti.Last
		jsRes.QuoteRes[num].Change = wti.Change
		jsRes.QuoteRes[num].High = wti.High
		jsRes.QuoteRes[num].Low = wti.Low
		jsRes.QuoteRes[num].Name = "USDWWW"
		num++
	}

	var micexUsd *Quote
	var micexEur *Quote
	if !withoutmicex {
		micexUsd = getMicexQuoteWithKey("USD000UTSTOM")
		micexEur = getMicexQuoteWithKey("EUR_RUB__TOM")
	} else {
		micexEur = getProductQuotesWithKeyNew("currencies", "eur-rub", "1691")
		if micexEur != nil {
			fmt.Printf("\nEUR MICEX %s", micexEur.Last)
		}
	}
	micex = false


	if withyql {
		for i := 0; i < countYQL; i++ {
			qname := strings.Replace(jsYQL.Query.Results.Quote[i].Name, "/", "", -1)
			if qname == "USDRUB" && micexUsd != nil {
				fmt.Printf("\nUSD MICEX %s", micexUsd.Last)
				jsRes.QuoteRes[num].LastTradePriceOnly = micexUsd.Last
				jsRes.QuoteRes[num].Change = micexUsd.Change
				jsRes.QuoteRes[num].High = micexUsd.High
				jsRes.QuoteRes[num].Low = micexUsd.Low
				micex = true

			} else if qname == "EURRUB" && micexEur != nil {
				fmt.Printf("\nEUR MICEX %s", micexEur.Last)
				jsRes.QuoteRes[num].LastTradePriceOnly = micexEur.Last
				jsRes.QuoteRes[num].Change = micexEur.Change
				jsRes.QuoteRes[num].High = micexEur.High
				jsRes.QuoteRes[num].Low = micexEur.Low
				micex = true

			} else  {
				jsRes.QuoteRes[num].LastTradePriceOnly = jsYQL.Query.Results.Quote[i].LastTradePriceOnly
				jsRes.QuoteRes[num].Change = jsYQL.Query.Results.Quote[i].Change
				jsRes.QuoteRes[num].High = jsYQL.Query.Results.Quote[i].DaysHigh
				jsRes.QuoteRes[num].Low = jsYQL.Query.Results.Quote[i].DaysLow
				fmt.Printf("\n%s %s", qname, jsRes.QuoteRes[num].LastTradePriceOnly)

			}
			jsRes.QuoteRes[num].Name = qname
			num++
		}
	} else {
		for i := 0; i < countYQL; i++ {
			qname := invQuote[i].Name
			fmt.Printf("\n%s", qname)
			if qname == "USDRUB" && micexUsd != nil {
				fmt.Printf("\nUSD MICEX %s", micexUsd.Last)
				jsRes.QuoteRes[num].LastTradePriceOnly = micexUsd.Last
				jsRes.QuoteRes[num].Change = micexUsd.Change
				jsRes.QuoteRes[num].High = micexUsd.High
				jsRes.QuoteRes[num].Low = micexUsd.Low
				micex = true

			} else  {
				jsRes.QuoteRes[num].LastTradePriceOnly = invQuote[i].Last
				jsRes.QuoteRes[num].Change = invQuote[i].Change
				jsRes.QuoteRes[num].High = invQuote[i].High
				jsRes.QuoteRes[num].Low = invQuote[i].Low
				fmt.Printf("\n%s %s", qname, jsRes.QuoteRes[num].LastTradePriceOnly)

			}
			jsRes.QuoteRes[num].Name = qname
			num++
		}
		if micexEur != nil {
			fmt.Printf("\nEUR MICEX %s", micexEur.Last)
			jsRes.QuoteRes[num].LastTradePriceOnly = micexEur.Last
			jsRes.QuoteRes[num].Change = micexEur.Change
			jsRes.QuoteRes[num].High = micexEur.High
			jsRes.QuoteRes[num].Low = micexEur.Low
			jsRes.QuoteRes[num].Name = "EURRUB"
			micex = true
			num++

		}
		//		else {
		//			var quoteUsdRubInv *QuoteInv
		//			var quoteEurUsdInv *QuoteInv
		//			for i := 0; i < countYQL; i++ {
		//				qname := invQuote[i].Name
		////				fmt.Printf("\n%s", qname)
		//				if qname == "USDRUB" {
		//					quoteUsdRubInv = invQuote[i]
		//
		//				} else if qname == "USDEUR" {
		//					quoteEurUsdInv = invQuote[i]
		//				}
		//			}
		//			if quoteEurUsdInv != nil && quoteUsdRubInv != nil  {
		//				lastUsdRubF, _ := strconv.ParseFloat(quoteUsdRubInv.Last, 64)
		//				changeUsdRubF, _ := strconv.ParseFloat(quoteUsdRubInv.Change, 64)
		//				highUsdRubF, _ := strconv.ParseFloat(quoteUsdRubInv.High, 64)
		//				lowUsdRubF, _ := strconv.ParseFloat(quoteUsdRubInv.Low, 64)
		//
		//				lastEurUsdF, _ := strconv.ParseFloat(quoteEurUsdInv.Last, 64)
		//				changeEurUsdF, _ := strconv.ParseFloat(quoteEurUsdInv.Change, 64)
		//				highEurUsdF, _ := strconv.ParseFloat(quoteEurUsdInv.High, 64)
		//				lowEurUsdF, _ := strconv.ParseFloat(quoteEurUsdInv.Low, 64)
		//
		//				openUsdRubF := lastUsdRubF/(1 + (changeUsdRubF/100))
		//				openEurUsdF := lastEurUsdF/(1 + (changeEurUsdF/100))
		//
		//				openEurRubF := openUsdRubF/openEurUsdF
		//				fmt.Printf("\nOPEN %f", changeEurUsdF)
		//
		//				lastEurRubF := lastUsdRubF/lastEurUsdF
		//				changeEurRubF := ((lastEurRubF/openEurRubF) - 1)*100
		//				higEurRubF := highUsdRubF/highEurUsdF
		//				lowEurRubF := lowUsdRubF/lowEurUsdF
		//
		//				q := new(QuoteInv)
		//				q.Last = strconv.FormatFloat(lastEurRubF, 'f', 15, 64)
		//				q.High = strconv.FormatFloat(higEurRubF, 'f', 15, 64)
		//				q.Low = strconv.FormatFloat(lowEurRubF, 'f', 15, 64)
		//				q.Change = strconv.FormatFloat(changeEurRubF, 'f', 15, 64)
		//				q.Name = "EURRUB"
		//
		//				jsRes.QuoteRes[num].LastTradePriceOnly = q.Last
		//				jsRes.QuoteRes[num].Change = q.Change
		//				jsRes.QuoteRes[num].High = q.High
		//				jsRes.QuoteRes[num].Low = q.Low
		//				jsRes.QuoteRes[num].Name = q.Name
		//				num++
		//				fmt.Printf("\nEURRUB %s", q.Last)
		//			}
		//		}
	}

	fmt.Printf("%i", num)
	succeeded = false
	if num >= 19 {
		b, err := json.Marshal(jsRes)
		if err != nil {
			fmt.Println("error:", err)
		} else {
			sendQuotes(b)
			succeeded = true
		}
	}
	return succeeded, micex
}

func sendQuotes(b []byte)  {
	fmt.Printf("sendQuotes")

	req, err := http.NewRequest("POST", "https://i-dialect.com/rubl/quotes/qrecin.php", bytes.NewBuffer(b))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("User-Agent", "Mozilla/5.0 (iPad; U; CPU OS 9_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B")
	//	reqb, _ := ioutil.ReadAll(req.Body)
	//	fmt.Println("\nreq:", string(reqb))

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		fmt.Printf("%s", err)
	} else {
		defer resp.Body.Close()

		fmt.Println("\nresponse Status:", resp.Status)
		//fmt.Println("response Headers:", resp.Header)
		body, _ := ioutil.ReadAll(resp.Body)
		fmt.Println("\nresponse Body:", string(body))
	}
}

//func main() {
//
//	suc, m := StartParseQuotes(true, false, false)
//	fmt.Printf("\n%b", suc)
//	fmt.Printf("\n%b", m)
//}
